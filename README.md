# Acerca de este Proyecto

Esta aplicacion fue realizada con webcomponenetes basados en Polymer 2 y LitElement, generados con la plataforma de CELLS. La version del Cells Cli fue la 3.2.1.

## Qué contiene?

- Para la autenticacion se utilizó el componente [polymerfire](https://www.webcomponents.org/element/FirebaseExtended/polymerfire) para gestionar el login por correo electrónico o cuenta de usuario de Gmail, Facebook y Twitter.
- Se cuenta con una interfaz para crear una cuenta de ahorro
- Se puede conocer el estado de conectado o desconectado de los usuarios q usan la aplicación
- Se pueden consultar los movimientos de una cuenta
- Se pueden crear movimientos
- Se usaron componentes desde el catalogo de Cells: [Cells Catalog](https://catalogs.platform.bbva.com/cells/) - BBVA Experience & Cells Architecture.
- Al ser una aplicacion híbrida (Polymer 2 y LitElement) se gestionan las dependencias con **bower** y **npm**.

# CELLS (**cells-cli**)

Se debe tener instalado una version de **cells-cli** >= 3.2.x.

## Instalación

1.- Previamente se debe descargar e instalar el **backend** y ejecutarlo de forma local en el puerto 3001 ubicado en el siguiente [repositorio](https://bitbucket.org/mlc_2020/backend/src/master/).

2.- Descarga de dependencias: 

~~~sh
npm install
~~~

~~~sh
bower i
~~~

Una vez instalado verificar que la carpeta `components` y `node_modules` se hayan creado dentro del proyecto.

## Ejecución

Abrir un terminal dentro del proyecto y ejecutar:

~~~
$ cells app:serve -c dev.json
~~~


