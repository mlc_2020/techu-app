(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'dashboard': '/dashboard',
      'movement': '/movements',
      'help': '/help'
    }
  });
}());
