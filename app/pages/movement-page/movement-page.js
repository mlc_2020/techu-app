import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-list-movement';
import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-button-floating-action';
import '@bbva-web-components/bbva-form-text';
import '@bbva-web-components/bbva-form-select';
import { getMovements, setMovement, cleanUp } from '../../elements/movements-dm/movements-dm.js';
import { normalizeUriString } from '../../elements/utils/text.js';

class MovementPage extends i18n(CellsPage) {
  static get is() {
    return 'movement-page';
  }

  constructor() {
    super();
    this.movements = [];
    this.user = {};
    this.description = '';
    this.comerce = '';
    this.amount = '';
  }

  static get properties() {
    return {
      user: { type: Object },
      userName: { type: String },
      movements: { type: Array },
      description: { type: String },
      comerce: { type: String },
      amount: { type: String },
      currency: { type: String }
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._logoutModal = this.shadowRoot.querySelector('#logoutModal');
  }

  onPageEnter() {
    this.subscribe('user_data', (userName) => this.user = userName);

    if (!this.movements.length) {
      getMovements(false, this.user).then((movements) => {
        this.movements = movements;
        this.slideToggle();
      });
    }
    console.log('this.user in movements:', this.user);
    this.userName =  `Bienvenido, ${this.user.first_name}`;
    this.userImage = this.user.image;
  }

  onPageLeave() {

  }

  slideToggle() {
    let slideOpen = true;
    let heightChecked = false;
    let initHeight = 0;
    let mdiv = this.shadowRoot.querySelector('#spinner');
    if (!heightChecked) {
      initHeight = mdiv.offsetHeight;
      heightChecked = true;
    }
    if (slideOpen) {
      slideOpen = false;
      mdiv.style.height = '0px';
      mdiv.style.padding = '0px';
      mdiv.style.margin = '0px';
    } else {
      slideOpen = true;
      mdiv.style.height = initHeight + 'px';
    }
  }

  handleMovementClick({ id, label }) {
    this.publish('movement_title', label);
    this.navigate('movement-detail', { id, label: normalizeUriString(label), });
  }

  get movementList() {
    if (!this.movements.length || this.movements.length === 0) {
      return html `<p>Cuenta sin movimientos</p>`;
    }

    return this.movements.map((movement) => {
      const movementProperties = this.buildMovementProperties(movement);

      return html`
        <bbva-list-movement
          ...="${spread(movementProperties)}">
        </bbva-list-movement>
      `;
    });
  }

  buildMovementProperties(movement) {
    const { descripcion, comercio, monto, moneda } = movement;

    return {
      ...(descripcion && { 'description': descripcion }),
      ...(comercio && { 'card-title': comercio }),
      ...(monto && { 'amount': monto, 'local-currency': moneda, 'currency-code': moneda }),
      'class': 'bbva-global-semidivider',
      'aria-label': 'Ver detalle del pago con tarjeta',
      'language': 'es',
    };
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed" header-fixed>
        <div slot="app__header">
          <div class="container-image"> 
            <img .src="${this.userImage}" class="profile-image" />
          </div>
          <bbva-header-main
            icon-left1="coronita:return-12"
            accessibility-text-icon-left1="Cerrar Sesión"
            @header-icon-left1-click=${() => window.history.back()}
            icon-right1="coronita:team"
            @header-icon-right1-click=${() => this.navigate('help')}
            text=${this.userName}> 
          </bbva-header-main> 
          
        </div>

        <div slot="app__main" class="container">
          <div class="container-spinner" id="spinner"><bbva-spinner ></bbva-spinner></div>
          <bbva-button-floating-action
            variant="medium-blue"
            aria-label="Agregar Movimiento"
            icon="coronita:add"
            @click="${() => this.shadowRoot.querySelector('#movementModal').open()}"
          >
          </bbva-button-floating-action>

          ${this.movementList ? html`${this.movementList}` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
        
          <bbva-help-modal
            id="logoutModal" 
            header-icon="coronita:info" 
            header-text=${this.t('dashboard-page.logout-modal.header')}
            button-text=${this.t('dashboard-page.logout-modal.button')}
            @help-modal-footer-button-click=${() => window.cells.logout()}>
            <div slot="slot-content">
              <span>${this.t('dashboard-page.logout-modal.slot')}</span>
            </div>
          </bbva-help-modal>

          <bbva-help-modal
            id="movementModal" 
            header-icon="coronita:info" 
            header-text=${this.t('dashboard-page.movement-modal.header')}
            button-text=${this.t('dashboard-page.movement-modal.button')}
            @help-modal-footer-button-click=${this.registerMovement}>
            <div slot="slot-content">
              <div class="row">
                <bbva-form-text label="Descripcion" id="txtDescription" ></bbva-form-text>
                <bbva-form-text label="Comercio" id="txtComerce" ></bbva-form-text>
              </div>
              <div class="row">
                <bbva-form-text label="Monto" id="txtAmount" ></bbva-form-text>
                <bbva-form-select id="selectCurrency" @change="${this.currencyChanged}" optional-label='' label="Moneda">
                  <bbva-form-option value="PEN">Soles</bbva-form-option>
                  <bbva-form-option value="USD">Dólares</bbva-form-option>
                  <bbva-form-option value="EUR">Euros</bbva-form-option>
                </bbva-form-select>
              </div>
              <span>${this.t('dashboard-page.movement-modal.slot')}</span>
            </div>
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  registerMovement() {

    this.description = this.shadowRoot.querySelector('#txtDescription').value;
    this.comerce = this.shadowRoot.querySelector('#txtComerce').value;
    this.amount = this.shadowRoot.querySelector('#txtAmount').value;
    this.currency = this.shadowRoot.querySelector('#selectCurrency')._selectedOption.value;
    console.log('Des:', this.description);
    console.log('Comercio:', this.comerce);
    console.log('Monto:', this.amount);
    console.log('Monto:', this.currency);
    console.log('User:', this.user);
    const movement = {
      descripcion: this.description,
      comercio: this.comerce,
      monto: this.amount,
      moneda: this.currency,
      id_user: this.user.id_user,
      id_account: this.user.product
    };
    setMovement(false, movement).then((mov) => {
      cleanUp();
      getMovements(false, this.user).then((movements) => {
        this.movements = movements;
        cleanUp();
      });
    });
  }

  static get styles() {
    return css`
    bbva-header-main {
      --bbva-header-main-bg-color: #002171;
      --bbva-header-main-min-height: 100px;
      --bbva-header-main-text-top: 25px;
    }

    cells-template-paper-drawer-panel {
      background-color: #5472d3;
    }

    .profile-image {
      border-radius: 50%;
      height: 90px;
    }

    .container-image {
      margin: 0 auto;
      float: right;
      height: 95px;
      padding-top: 5px;
      background-color: #002171;
    }

    .row {
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      width: 98%;
    }

    bbva-button-floating-action {
      position: fixed;
    }

    bbva-form-text, bbva-form-select{
      margin: 5px;
    }

    .container-spinner{
      position: fixed;
      z-index: 2;
      background-color: #002171;
      width: 100%;
      height: 100%;
      padding-top: 50%;
      -webkit-transition: all 1s ease-in-out;
      transition: all 1s ease-in-out;
      overflow-y: hidden;
      margin-top: 0px;
    }
    `;
  }
}

window.customElements.define(MovementPage.is, MovementPage);