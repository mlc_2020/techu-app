import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-list-movement';
import '@bbva-web-components/bbva-list-card';
import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-card-advice';

import { getAccounts, cleanUp } from '../../elements/accounts-dm/accounts-dm.js';
import '../../elements/user-dm/user-dm.js';
import { normalizeUriString } from '../../elements/utils/text.js';

class DashboardPage extends i18n(CellsPage) {
  static get is() {
    return 'dashboard-page';
  }

  constructor() {
    super();

    this.accounts = [];
    this.user = {};
  }

  static get properties() {
    return {
      user: { type: Object },
      userName: { type: String },
      accounts: { type: Array },
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._logoutModal = this.shadowRoot.querySelector('#logoutModal');
  }

  onPageEnter() {
    this.subscribe('user_name', (userName) => this.user = userName);

    if (!this.accounts.length) {
      getAccounts(false, this.user).then((accounts) => {
        this.accounts = accounts;
        this.slideToggle();
      });
    }
    console.log('this.user in dashboard:', this.user);
    this.userName =  `Bienvenido, ${this.user.first_name}`;
    this.userImage = this.user.image;
  }

  onPageLeave() {

  }

  handleMovementClick({ id, numProduct }) {
    this.user.product = numProduct;
    this.publish('user_data', this.user);
    this.navigate('movement', { id, product: numProduct, });
  }

  getProduct(evt, product) {
    console.log('event::', evt);
    console.log('product:', product);
    let account = {};
    switch (product) {
      case 'CS':
        account = {
          id: 1,
          amount: 500,
          cardTitle: 'CUENTA SUELDO',
          contentText: 'PEN',
          numProduct: this.numberAccount(15),
          entity: 'bbva',
          entityAccessibilityText: 'BBVA'
        };
        break;
      case 'CI':
        account = {
          id: 2,
          amount: 1000,
          cardTitle: 'CUENTA INDEPENDENCIA',
          contentText: 'PEN',
          numProduct: this.numberAccount(15),
          entity: 'bbva',
          entityAccessibilityText: 'BBVA'
        };
        break;
    }
    this.user.accounts = [ account ];
    this.shadowRoot.querySelector('#userDm').setAccounts(this.user);
  }

  numberAccount(length) {
    let result = '';
    const characters = '0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  get accountList() {

    console.log('this.accounts:::', this.accounts);
    if (!this.accounts.length || this.accounts.length === 0) {
      return html`<bbva-card-advice
      @card-advice-item-click="${(e) => this.getProduct(e, 'CS')}"
      link-icon="coronita:wallet"
      image-url="resources/images/img2.png"
      link-text="Adquirir Producto"
      text="Abre tu CUENTA SUELDO y participa del sorteo para las entradas del Perú vs Chile."
    >
    </bbva-card-advice>
    <bbva-card-advice
      link-icon="coronita:wallet"
      @card-advice-item-click="${ (e) => this.getProduct(e, 'CI')}"
      image-url="resources/images/img2.png"
      link-text="Adquirir Producto"
      text="Abre tu CUENTA INDEPENDENCIA y participa del sorteo para las entradas del Perú vs Chile."
    >
    </bbva-card-advice>`;
    }


    return this.accounts.map((account) => {
      const accountProperties = this.buildAccountProperties(account);

      return html`
        <bbva-list-card
          ...="${spread(accountProperties)}">
        </bbva-list-card>
      `;
    });
  }

  updateListAccount() {
    this.accounts = this.user.accounts;
  }

  buildAccountProperties(account) {
    this.slideToggle();
    const { amount, cardTitle, contentText, numProduct, entity, entityAccessibilityText } = account;

    return {
      ...(contentText && { 'content-text': contentText }),
      ...(cardTitle && { 'card-title': cardTitle }),
      ...(amount && { 'amount': amount }),
      ...(numProduct && { 'num-product': numProduct}),
      ...(entity && { 'entity': entity }),
      ...(entityAccessibilityText && { 'entity-accessibility-text': entityAccessibilityText}),
      '@click': () => this.handleMovementClick(account),
      'class': 'bbva-global-semidivider',
      'aria-label': 'Ver detalle del pago con tarjeta',
      'language': 'es',
    };
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed" header-fixed>
        <div slot="app__header">
          <div class="container-image"> 
            <img .src="${this.userImage}" class="profile-image" />
          </div>
          <bbva-header-main
            icon-left1="coronita:on"
            accessibility-text-icon-left1="Cerrar Sesión"
            @header-icon-left1-click=${() => this._logoutModal.open()}
            icon-right1="coronita:team"
            accessibility-text-icon-right1="Ayuda" 
            @header-icon-right1-click=${() => this.navigate('help')}
            text=${this.userName}> 
          </bbva-header-main> 
          
        </div>

        <div slot="app__main" class="container">
          <div class="container-spinner" id="spinner"><bbva-spinner ></bbva-spinner></div>

          ${this.accountList ? html`${this.accountList}` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
        
          <bbva-help-modal
            id="logoutModal" 
            header-icon="coronita:info" 
            header-text=${this.t('dashboard-page.logout-modal.header')}
            button-text=${this.t('dashboard-page.logout-modal.button')}
            @help-modal-footer-button-click=${this.logout}>
            <div slot="slot-content">
              <span>${this.t('dashboard-page.logout-modal.slot')}</span>
            </div>
          </bbva-help-modal>
        </div>
        <div slot="app__overlay">
          <user-dm id="userDm" @user-dm-updated="${this.updateListAccount}"></user-dm>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  logout() {
    this.shadowRoot.querySelector('#userDm').logout(this.user);
    cleanUp();
    window.cells.logout();
  }

  slideToggle() {
    let slideOpen = true;
    let heightChecked = false;
    let initHeight = 0;
    let mdiv = this.shadowRoot.querySelector('#spinner');
    if (!heightChecked) {
      initHeight = mdiv.offsetHeight;
      heightChecked = true;
    }
    if (slideOpen) {
      slideOpen = false;
      mdiv.style.height = '0px';
      mdiv.style.padding = '0px';
      mdiv.style.margin = '0px';
    } else {
      slideOpen = true;
      mdiv.style.height = initHeight + 'px';
    }
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
        --bbva-header-main-min-height: 100px;
        --bbva-header-main-text-top: 25px;
      }
 
      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .profile-image {
        border-radius: 50%;
        height: 90px;
      }

      .container-image {
        margin: 0 auto;
        float: right;
        height: 95px;
        padding-top: 5px;
        background-color: #002171;
      }

      bbva-card-advice {
        --bbva-card-advice-bg-color: #fff;
        --bbva-card-advice-color: #121212;
        --bbva-card-advice-link-color: #1973b8;
      }

      bbva-card-advice { 
        padding: 5px; 
        width: 50%;
        float:left;
      }

      .container-spinner{
        position: fixed;
        z-index: 2;
        background-color: #002171;
        width: 100%;
        height: 100%;
        padding-top: 50%;
        -webkit-transition: all 1s ease-in-out;
        transition: all 1s ease-in-out;
        overflow-y: hidden;
        margin-top: 0px;
      }

      @media screen and (max-width: 360px) {
        bbva-card-advice { 
          padding: 5px; 
          width: 100%;
        }
      }
    `;
  }
}

window.customElements.define(DashboardPage.is, DashboardPage);