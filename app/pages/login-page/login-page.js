import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';
import '@cells-components/cells-button';
import '@bbva-web-components/bbva-core-generic-dp/bbva-core-generic-dp';

import { cleanUp } from '../../elements/movements-dm/movements-dm';

class LoginPage extends i18n(CellsPage) {
  static get is() {
    return 'login-page';
  }

  static get properties() {
    return {
      _optionsList: { type: Array },
      _path: { type: String },
      _method: { type: String },
      _user: { type: Object },
      canContinue: { type: Boolean },
      _hideSpinner: { type: Boolean }
    };
  }

  get _configAuthDomain() {
    return window.AppConfig ? window.AppConfig.authDomain : '';
  }

  get _configFirebaseDb() {
    return window.AppConfig ? window.AppConfig.firebaseDb : '';
  }

  get _configFirebaseApiKey() {
    return window.AppConfig ? window.AppConfig.firebaseApiKey : '';
  }

  get _configFirebaseStorageBucket() {
    return window.AppConfig ? window.AppConfig.firebaseStorageBucket : '';
  }

  get _configMessagingSenderId() {
    return window.AppConfig ? window.AppConfig.messagingSenderId : '';
  }

  get _configHostBackend() {
    return window.AppConfig ? window.AppConfig.hostBackend : '';
  }

  get _configPathSearchUser() {
    return window.AppConfig ? window.AppConfig.pathSearchUser : '';
  }

  get _configPathNewUser() {
    return window.AppConfig ? window.AppConfig.pathNewUser : '';
  }

  get _configPathLoginUser() {
    return window.AppConfig ? window.AppConfig.loginUser : '';
  }

  constructor() {
    super();
    this._optionsList = [
      { 'provider': 'google', 'icon': 'coronita:googleplus2', 'text': 'login-page.button.gmail' },
      { 'provider': 'facebook', 'icon': 'coronita:facebook2', 'text': 'login-page.button.fb' },
      { 'provider': 'twitter', 'icon': 'coronita:twitter2', 'text': 'login-page.button.twitter' }
    ];
    this._path = '';
    this._method = '';
    this._user = {};
    this.canContinue = false;
    this._hideSpinner = false;
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._userInput = this.shadowRoot.querySelector('#user');
    this._passwordInput = this.shadowRoot.querySelector('#password');

  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            text="BBVA"">
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <div class="container-spinner" id="spinner"><bbva-spinner ></bbva-spinner></div>
          <firebase-app
            auth-domain="${this._configAuthDomain}"
            database-url="${this._configFirebaseDb}"
            api-key="${this._configFirebaseApiKey}"
            storage-bucket="${this._configFirebaseStorageBucket}"
            messaging-sender-id="${this._configMessagingSenderId}">
          </firebase-app>
        
          ${this._optionsList.map(e => {
    return html`<bbva-button-default class="${e.provider}" .option="${e.provider}"
            @click=${this.login}>
            <cells-icon icon="${e.icon}"></cells-icon>
            &nbsp;&nbsp;${this.t(e.text)} 
          </bbva-button-default></div></div>
          <firebase-auth id="${e.provider}" user="${this.user}" provider="${e.provider}" on-error="${this.handleError}">
          </firebase-auth>`;
  })}
        </div>
        <div slot="app__overlay">
          <bbva-core-generic-dp 
          id="userDp"
          cross-domain=true
          host="${this._configHostBackend}"
          path="${this._path}"
          method="${this._method}"
          @request-success="${ this._onResponse}"
          @request-error="${ this._onError}"
          ></bbva-core-generic-dp>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  login(evt) {
    const { option } = evt.target;
    const element = this.shadowRoot.querySelector(`#${option}`);
    let _this = this;
    if (element) {
      element.signInWithPopup()
        .then(function(response) {
          _this.validateUserProvider(option, response);
          console.log('response firebase:', response);
        })
        .catch(function(error) {
          console.log('error firebase:', error);
        });
    }
  }

  validateUserProvider(option, response) {
    console.log('validateUserProvider::', response);
    switch (option) {
      case 'google':
        this._user = {
          email: response.additionalUserInfo.profile.email,
          first_name: response.additionalUserInfo.profile.given_name,
          last_name: response.additionalUserInfo.profile.family_name,
          password: '',
          image: response.additionalUserInfo.profile.picture
        };
        break;
      case 'facebook':
        this._user = {
          email: response.additionalUserInfo.profile.email,
          first_name: response.additionalUserInfo.profile.first_name,
          last_name: response.additionalUserInfo.profile.last_name,
          password: '',
          image: response.additionalUserInfo.profile.picture.data.url
        };
        break;
      case 'twitter':
        this._user = {
          email: response.additionalUserInfo.username,
          first_name: response.additionalUserInfo.profile.name.split(' ')[0],
          last_name: response.additionalUserInfo.profile.name.split(' ')[1],
          password: '',
          image: response.additionalUserInfo.profile.profile_image_url
        };
        break;
    }

    const userDp = this.shadowRoot.querySelector('#userDp');
    userDp.path = this._configPathSearchUser.replace('{0}', this._user.email);
    userDp.method = 'GET';
    userDp.generateRequest();
  }

  _onResponse(evt) {
    console.log('_onResponse:', evt);
    if (!this.canContinue) {
      if (evt.detail.user !== undefined) {
        this._user.id_user = evt.detail.user[0].id_user;
      } else {
        this._user.id_user = evt.detail.id_user;
      }

      const userDp = this.shadowRoot.querySelector('#userDp');
      userDp.path = this._configPathLoginUser;
      userDp.method = 'POST';
      userDp.body = {
        email: this._user.email,
        password: ''
      };
      this.canContinue = true;
      userDp.generateRequest();
      this.publish('user_name', this._user);
      this.navigate('dashboard');
    }
  }

  _onError(evt) {
    console.log('_onError:', evt);
    if (evt.detail.response !== null && evt.detail.response.msg === 'Usuario no encontrado') {
      this.registraNuevoUsuario();
    }
  }

  registraNuevoUsuario() {
    const userDp = this.shadowRoot.querySelector('#userDp');
    userDp.path = this._configPathNewUser;
    userDp.method = 'POST';
    this._user.accounts = [];
    userDp.body = this._user;
    userDp.generateRequest();
  }

  onPageEnter() {
    cleanUp();
    setTimeout(() =>  this.slideToggle(), 3 * 1000);
  }

  onPageLeave() {
    // Cada vez que salgamos del login, limpiamos las cajas de texto.
    // setTimeout(() => [this._userInput, this._passwordInput].forEach((el) => el.clearInput()), 3 * 1000);
  }

  slideToggle() {
    let slideOpen = true;
    let heightChecked = false;
    let initHeight = 0;
    let mdiv = this.shadowRoot.querySelector('#spinner');
    if (!heightChecked) {
      initHeight = mdiv.offsetHeight;
      heightChecked = true;
    }
    if (slideOpen) {
      slideOpen = false;
      mdiv.style.height = '0px';
      mdiv.style.padding = '0px';
      mdiv.style.margin = '0px';
    } else {
      slideOpen = true;
      mdiv.style.height = initHeight + 'px';
    }
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .container > * {
        margin-top: 10px;
      }

      .google {
        background-color: #e94235;
        width:250px;
      }

      .facebook{
        background-color: #0d8af0;
        width:250px;
      }

      .twitter{
        background-color: #1da1f2;
        width:250px;
      }

      .row {
        display: grid;
        padding: 5px;
        grid-template-columns: repeat(2, 1fr);
        width: 100%;
      }

      .container-spinner{
        position: fixed;
        z-index: 2;
        background-color: #002171;
        width: 100%;
        height: 100%;
        padding-top: 50%;
        -webkit-transition: all 1s ease-in-out;
        transition: all 1s ease-in-out;
        overflow-y: hidden;
        margin-top: 0px;
      }
    `;
  }
}

window.customElements.define(LoginPage.is, LoginPage);