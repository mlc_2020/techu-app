import { CellsPage } from '@cells/cells-page';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-list-info';
import '@bbva-web-components/bbva-text/bbva-text.js';
import '@bbva-web-components/bbva-card-contact';
import '../../elements/user-dm/user-dm.js';

class HelpPage extends CellsPage {
  static get is() {
    return 'help-page';
  }

  static get properties() {
    return {
      users: { type: Array }
    };
  }

  constructor() {
    super();
    this.users = [];
  }

  onPageEnter() {
    if (!this.users.length) {
      /*getAccounts(false, this.user).then((accounts) => {
        this.accounts = accounts;
      });*/

    }
    this.shadowRoot.querySelector('#userDm').listUsers();
  }

  firstUpdated() {
    super.firstUpdated();
    console.log('this.users.length::', this.users.length);

  }

  listUserRender() {

    let _list = this.users === null || Object.keys(this.users).length === 0 || this.users.length === 0 ? [ { first_name: '' } ] : this.users;

    console.log('this.users.length :::', this.users.length);
    if (this.users.length > 0) {
      _list.map(e => {
        return html`<div>${e.first_name}</div>`;
      });
    }
  }

  render() {

    let _list = this.users === null || Object.keys(this.users).length === 0 || this.users.length === 0 ? [ { first_name: '' } ] : this.users;

    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            text="Listado de Usuarios"
            icon-left1="coronita:return-12"
            accessibility-text-icon-left1="Volver"
            @header-icon-left1-click=${() => window.history.back()}>
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
        <div class="legend">
          <cells-icon icon="coronita:myprofile" class="status-online"></cells-icon> En Línea
          <cells-icon icon="coronita:myprofile" class="status-offline"></cells-icon> Desconectado
        </div>
    ${_list.map(e => {
    return html`<bbva-card-contact class="status ${e.logged}" name="${e.first_name} ${e.last_name}" info="${e.email}"></bbva-card-contact>`;
  })}
        </div>
        <div slot="app__overlay" >
          <user-dm id="userDm" @user-dm-list="${this.setUsers}"></user-dm>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  setUsers(users) {
    this.users = users.detail;
    console.log('mis usuarios:', this.users);
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .status{
        background-color: #f3efef;
      }

      .status-online{
        color: #97ffc2;
      }

      .status-offline{
        color: #f3efef;
      }

      .status.true {
        background-color: #97ffc2;
      }

      bbva-card-contact{
        width: 25%;
        float: left;
      }

      .legend {
        width: 100%;
      }

      @media screen and (max-width: 360px) {
        bbva-card-contact{
          width: 50%;
          float: left;
        }
      }
    `;
  }
}

window.customElements.define(HelpPage.is, HelpPage);