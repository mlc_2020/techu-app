let fetched = false;
let accounts = [];

export async function getAccounts(force = false, user) {
  console.log('user in dm', user);
  if (fetched || fetched && !force) {
    return Promise.resolve(accounts);
  }

  const { hostBackend, pathAccounts } = window.AppConfig;
  let path = pathAccounts.replace('{0}', user.id_user);
  const endpoint = `${hostBackend}/${path}`;
  const result = await fetch(endpoint).then((response) => response.json());

  if (result.accounts !== undefined) {
    accounts = result.accounts;
  }
  fetched = true;
  return accounts;
}

export function cleanUp() {
  fetched = false;
  accounts = [];
}