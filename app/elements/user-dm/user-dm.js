import { LitElement, html, } from 'lit-element';
import '@bbva-web-components/bbva-core-generic-dp/bbva-core-generic-dp';

export class UserDm extends LitElement {
  static get is() {
    return 'user-dm';
  }

  // Declare properties
  static get properties() {
    return {
      _body: { type: Object },
      _evt: { type: String },
      _defaultData: { type: Object }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this._body = {};
    this._evt = '';
    this._defaultData = {};
  }

  get _configHost() {
    return window.AppConfig ? window.AppConfig.hostBackend : '';
  }

  get _configPathUpdateUserAttribute() {
    return window.AppConfig ? window.AppConfig.pathUpdateUserAttribute : '';
  }

  get _configPathLogoutUser() {
    return window.AppConfig ? window.AppConfig.logoutUser : '';
  }

  get _configPathAllUsers() {
    return window.AppConfig ? window.AppConfig.pathAllUsers : '';
  }

  logout(user) {
    this._evt = 'logout';
    let dp = this.shadowRoot.querySelector('#myDataProvider');
    if (dp !== null) {
      dp.path = this._configPathLogoutUser;
      dp.body = { email: user.email };
      dp.method = 'POST';
      dp.generateRequest();
    }
  }

  setAccounts(evt) {
    this._evt = 'setAccounts';
    console.log('setAccounts', evt);
    this._defaultData = evt;
    let dp = this.shadowRoot.querySelector('#myDataProvider');
    if (dp !== null) {
      dp.path = this._configPathUpdateUserAttribute.replace('{0}', evt.id_user);
      dp.body = evt;
      dp.method = 'PATCH';
      dp.generateRequest();
    }
  }

  listUsers() {
    this._evt = 'listUsers';
    let dp = this.shadowRoot.querySelector('#myDataProviderUsers');
    if (dp !== null) {
      dp.path = this._configPathAllUsers;
      dp.method = 'GET';
      dp.generateRequest();
    }
  }

  _onResponseUsers(evt) {
    let document = evt.detail;
    if (document) {
      switch (this._evt) {
        case 'listUsers':
          this.dispatchEvent(
            new CustomEvent('user-dm-list', {
              bubbles: true,
              composed: true,
              detail: document.users
            }),
          );
          break;

      }
    }
  }

  _onResponse(evt) {
    let document = evt.detail;
    if (document) {
      switch (this._evt) {
        case 'setAccounts':
          this.dispatchEvent(
            new CustomEvent('user-dm-updated', {
              bubbles: true,
              composed: true,
              detail: document
            }),
          );
          break;

      }
    }

  }

  _onError(evt) {
    console.log('_onError en user-dm:', evt);
  }

  _onErrorusers(evt) {
    console.log('_onErrorusers en user-dm:', evt);
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <bbva-core-generic-dp
        id="myDataProvider"
        cross-domain=true
        host="${this._configHost}"
        @request-success="${ this._onResponse}"
        @request-error="${ this._onError}">
      </bbva-core-generic-dp>

      <bbva-core-generic-dp
        id="myDataProviderUsers"
        host="${this._configHost}"
        @request-success="${ this._onResponseUsers}"
        @request-error="${ this._onErrorusers}">
      </bbva-core-generic-dp>
      `;
  }
}

// Register the element with the browser
customElements.define(UserDm.is, UserDm);