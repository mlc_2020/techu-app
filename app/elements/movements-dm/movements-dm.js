let fetched = false;
let fetchedMov = false;
let movements = [];
let mov = {};

export async function getMovements(force = false, user) {
  if (fetched || fetched && !force) {
    return Promise.resolve(movements);
  }

  const { hostBackend, pathMovements } = window.AppConfig;

  let path = pathMovements.replace('{0}', user.id_user).replace('{1}', user.product);
  const endpoint = `${hostBackend}/${path}`;
  const result = await fetch(endpoint).then((response) => response.json());
  if (result.movements !== undefined) {
    movements = result.movements;
  }
  fetched = true;
  return movements;
}

export async function setMovement(force = false, movement) {
  console.log('setMovement in dm:', movement);
  if (fetchedMov || fetchedMov && !force) {
    return Promise.resolve(mov);
  }

  const { hostBackend, pathNewMovements } = window.AppConfig;
  const endpoint = `${hostBackend}/${pathNewMovements}`;
  const result = await fetch(endpoint, {
    method: 'post',
    headers: {'Content-type': 'application/json;charset=UTF-8'},
    body: JSON.stringify(movement)
  }).then((response) => response.json());
  if (result.id_movimiento !== undefined) {
    mov = result;
  }
  fetchedMov = true;
  return mov;
}

export function cleanUp() {
  fetched = false;
  fetchedMov = false;
  movements = [];
  mov = {};
}
